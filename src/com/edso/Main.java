package com.edso;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        int N;
        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap độ dài của mảng : ");
        N = scanner.nextInt();
        double[] array = input(N);
        output(array, N);
        System.out.println("\n--------------");
        array = removeTheElement(array);
        System.out.println("\n--------------");
        System.out.println("Mảng kết quả:\n"
                + Arrays.toString(array));

    }

    public static double[] input(int N) {
        double[] array = new double[N];
        Scanner scanner = new Scanner(System.in);
        for (int i = 0; i < N; i++) {
            System.out.println("Nhập vào phần tử thứ " + (i) + ": ");
            array[i] = scanner.nextInt();
            if (array[i] < 1 || array[i] > 100) {
                System.out.println("bạn nhập sai");
                break;
            }
        }

        return array;
    }

    public static double[] removeTheElement(double[] arr) {
        int dem = arr.length;

        for (double v : arr) {
            if (v < 50) {
                dem--;
            }
        }
        double[] anotherArray = new double[dem];
        for (int i = 0, k = 0; i < arr.length; i++) {
            if (arr[i] < 50) {
                System.out.println("Phân tử bị xóa " + arr[i]);
                continue;
            }
            anotherArray[k++] = arr[i];
        }
        return anotherArray;
    }

    public static void output(double[] array, int N) {
        for (int i = 0; i < N; i++)
            System.out.print(array[i] + " ");
        System.out.print("");
    }
}






